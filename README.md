# Molecular Parser

For a given chemical formula represented by a string, write a function that counts the number of atoms of each element contained in the molecule and returns an object where keys correspond to atoms and values to the number of each atom in the molecule.

For example:
* The input 'H2O' must return {'H': 2, 'O': 1}

* The input 'Mg(OH)2' must return {'Mg': 1, 'O': 2, 'H': 2}

* The input 'K4[ON(SO3)2]2' must return {'K': 4, 'O': 14, 'N': 2, 'S': 4}

As you can see, some formulas have brackets in them. The index outside the brackets tells you that you have to multiply count of each atom inside the bracket on this index. For example, in Fe(NO3)2 you have one iron atom, two nitrogen atoms and six oxygen atoms.

Note that brackets may be round, square or curly and can also be nested. Index after the braces is optional.

## Requirements

Docker must be installed, all the rest is handled with containers.

Have an AWS account, and access/secret keys generated. 

## Run the app on dev envirement

Build the docker file

`make build`

Then up the docker container :

`make dev`

Test the app on your browser :

`http://localhost/parse/{molecular_formula}`

Ex :

* `http://localhost/parse/Fe(NO3)2`

* `http://localhost/parse/K4[ON(SO3)2]2`

## Useful Docker commands

**Build image :**

`make build`

**Up dev env :**

`make dev`

**Access to container :**

`make exec`

**Clean dev env :**

`make clean`

**Show logs :**

`make logs`


## Terraform 

### Create aws infra

Create ssh key 

`cd infra/terraform && ssh-keygen -f mykey`

You need to configure your aws environment. 

`aws configure`  

`docker pull hashicorp/terraform`

`docker run -it -v ${PWD}/infra/terraform:/terraform -w="/terraform" hashicorp/terraform init`

`docker run -it -v ${PWD}/infra/terraform:/terraform -v $HOME/.aws/credentials:/root/.aws/credentials -w="/terraform" hashicorp/terraform apply`

### Deploy application

`docker pull coxauto/aws-ebcli`

`docker run -it -e AWS_DEFAULT_REGION=eu-west-1 -e AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> -e AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY> -w /work -v $PWD:/work coxauto/aws-ebcli eb init`

`docker run -it -e AWS_DEFAULT_REGION=eu-west-1 -e AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> -e AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY> -w /work -v $PWD:/work coxauto/aws-ebcli eb deploy`

### Terminate application

`docker run -it -e AWS_DEFAULT_REGION=eu-west-1 -e AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> -e AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY> -w /work -v $PWD:/work coxauto/aws-ebcli eb terminate`

### Destroy infra
`docker run -it -v ${PWD}/infra/terraform:/terraform -v $HOME/.aws/credentials:/root/.aws/credentials -w="/terraform" hashicorp/terraform destroy`
