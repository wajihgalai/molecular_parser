build:
	docker-compose build molecular_parser

build-without-cache:
	docker-compose build --no-cache molecular_parser

test:
	docker-compose run --rm molecular_parser test

coverage:
	docker-compose run --rm molecular_parser coverage

dev:
	docker-compose up -d molecular_parser

exec:
	docker-compose exec molecular_parser sh

logs:
	docker-compose logs -f molecular_parser

clean:
	docker-compose kill
	docker-compose down -v

shell:
	docker-compose run --rm molecular_parser shell
