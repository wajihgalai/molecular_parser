#!/usr/bin/env sh

LAUNCH_SHELL=no
mode=""

for arg in $@
do
    if [ "$arg" = "shell" ]
    then
        LAUNCH_SHELL=yes
    elif [ "$arg" = "dev" ]
    then
        mode="dev"
    elif [ "$arg" = "test" ]
    then
        mode="test"
    fi
done

if [ "$LAUNCH_SHELL" = "yes" ]
then
    exec /bin/sh
elif [ "$mode" = "dev" ]
then
    exec s6-svscan /s6/
elif [ "$mode" = "test" ]
then
    exec vendor/bin/phpunit
else
    echo "I don't know what I have to do"
fi
