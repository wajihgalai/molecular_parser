<?php

namespace App\Http\Controllers;

use App\Services\MoleculeService;

class MoleculeController extends Controller
{

    public function parse($formula, MoleculeService $moleculeService)
    {
        $moleculeService->setFormula(urldecode($formula));

        if (!$matches = $moleculeService->isValid()) {
            return response()->json([
                'message' => 'Invalid formula'
            ], 400);
        }

        return $moleculeService->parse($matches);
    }
}
