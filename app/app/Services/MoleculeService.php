<?php

namespace App\Services;

class MoleculeService
{
    protected $formula = "";

    protected $atoms = [];

    /**
     * Check if all formula matches
     *
     * @return array|bool
     */
    public function isValid()
    {
        $matches = $this->explodeFormula($this->formula);

        return strlen(implode('', array_flatten($matches))) == strlen($this->formula) ? $matches : false;
    }

    /**
     * Parse a valid molecule formula and return a list of atoms
     *
     * @param     $matches
     * @param int $coefficient
     *
     * @return array
     */
    public function parse($matches, $coefficient = 1)
    {
        foreach ($matches as $match) {
            // Check if the matched value is an valid atom
            if ($atom = $this->isAtom($match)) {
                $this->addAtom($atom['name'], $atom['number'] * $coefficient);

                continue;
            }

            // Otherwise its a sub-formula
            $subFormula = $this->isolatedCoefficientAndFormula($match);

            $new_matches = $this->explodeFormula($subFormula['formula']);

            // Parse the matched sub-formula
            $this->parse($new_matches, $subFormula['coefficient'] * $coefficient);
        }

        return $this->atoms;
    }

    /**
     * Push atom name and number to atoms list
     *
     * @param $atom_name
     * @param $number
     */
    public function addAtom($atom_name, $number = 1)
    {
        isset($this->atoms[$atom_name]) ?
            $this->atoms[$atom_name] += (int) $number :
            $this->atoms[$atom_name] = (int) $number;
    }

    /**
     * Check if the given params is a valid atom
     *
     * @param $atom
     *
     * @return array|bool
     */
    public function isAtom($atom)
    {
        $checkAtomRegex = "/^([A-Z][a-z]?)(\d*)$/";

        if (preg_match($checkAtomRegex, $atom, $matches)) {

            return [
                'name' => $matches[1],
                'number' => !empty($matches[2]) ? (int) $matches[2] : 1
            ];
        }

        return false;
    }

    /**
     * Explode formula on atoms and sub-formulas
     * Ex 'Mg(OH)2' => ['Mg', '(OH)2']
     *
     * @param $formula
     *
     * @return array
     */
    public function explodeFormula($formula)
    {
        $regex = "/[A-Z][a-z]?\d*|[\(\[\{][^()\[\]]*(?:[\(\[\{].*[\)\]\}])?[^()\[\]]*[\)\]\}]\d+/";

        preg_match_all($regex, $formula, $matches);

        return $matches[0];
    }

    /**
     * Isolate the Coefficient from formula
     * Ex:
     * (OH)2 => [
     *      'formula' => 'OH',
     *      'coefficient' => 2
     * ]
     *
     * @param $formula
     *
     * @return array
     */
    public function isolatedCoefficientAndFormula($formula)
    {
        $regex = "/[\(\[\{]([^()\[\]]*(?:[\(\[\{].*[\)\]\}])?[^()\[\]]*)[\)\]\}](\d+)/";

        preg_match($regex, $formula, $matches);

        return [
            'formula' => $matches[1],
            'coefficient' => isset($matches[2]) ? (int) $matches[2] : 1
        ];
    }

    /**
     * Molecule formula setter
     *
     * @param $formula
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;
    }

    /**
     * Atoms getter
     */
    public function getAtoms()
    {
        return $this->atoms;
    }
}