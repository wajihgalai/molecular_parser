<?php

class MoleculeControllerTest extends TestCase
{
    public function testParse_success()
    {
        $response = $this->get('/parse/H2O');
        $response->assertResponseStatus(200);

        $response = $this->get('/parse/Mg(OH)2');
        $response->assertResponseStatus(200);

        $response = $this->get('/parse/K4[ON(SO3)2]2');
        $response->assertResponseStatus(200);

        $response = $this->get('/parse/(K4[ON{SO3}2]2)3');
        $response->assertResponseStatus(200);
    }

    public function testParse_fail()
    {
        $response = $this->get('/parse/H2o');
        $response->assertResponseStatus(400);

        $response = $this->get('/parse/H2$O');
        $response->assertResponseStatus(400);

        $response = $this->get('/parse/mg(OH)2');
        $response->assertResponseStatus(400);

        $response = $this->get('/parse/(())');
        $response->assertResponseStatus(400);
    }
}
