<?php

use App\Services\MoleculeService;

class MoleculeServiceTest extends TestCase
{
    public function inValidFormulas()
    {
        return [
            ['H2$O'],
            ['H2o'],
            ['(())'],
            ['Haa2'],
        ];
    }

    /**
     * @dataProvider inValidFormulas
     */
    public function testIsValid_fail($formula)
    {
        $molecule = new MoleculeService();

        $molecule->setFormula($formula);
        $result = $molecule->isValid();

        $this->assertFalse($result);
    }

    public function validFormulas()
    {
        return [
            [
                'Mg(OH)2',
                ['Mg', '(OH)2']
            ],
            [
                'K4[ON(SO3)2]2',
                ['K4', '[ON(SO3)2]2']
            ],
            [
                '(K4[ON{SO3}2]2)3',
                ['(K4[ON{SO3}2]2)3']
            ]
        ];
    }

    /**
     * @dataProvider validFormulas
     */
    public function testIsValid_success($formula, $expected)
    {
        $molecule = new MoleculeService();

        $molecule->setFormula($formula);
        $result = $molecule->isValid();

        $this->assertEquals($result, $expected);
    }

    public function addAtomProvider()
    {
        return [
            [
                'Mg',
                1,
                ['Mg' => 1]
            ],
            [
                'K',
                4,
                ['K' => 4]
            ],
        ];
    }

    /**
     * @dataProvider addAtomProvider
     */
    public function testAddAtom_success($atom_name, $number, $expected)
    {
        $molecule = new MoleculeService();

        $molecule->addAtom($atom_name, $number);

        $this->assertEquals($molecule->getAtoms(), $expected);
    }

    public function isAtomProvider()
    {
        return [
            [
                'Mg',
                [
                    'name' => 'Mg',
                    'number' => 1
                ]
            ],
            [
                'Mg2',
                [
                    'name' => 'Mg',
                    'number' => 2
                ]
            ],
            [
                'K',
                [
                    'name' => 'K',
                    'number' => 1
                ]
            ],
            [
                'K4',
                [
                    'name' => 'K',
                    'number' => 4
                ]
            ],
            [
                '(SO3)2',
                false
            ],
        ];
    }

    /**
     * @dataProvider isAtomProvider
     */
    public function testIsAtom_success($atom, $expected)
    {
        $molecule = new MoleculeService();

        $result = $molecule->isAtom($atom);

        $this->assertEquals($result, $expected);
    }

    public function explodeFormulaProvider()
    {
        return [
            [
                'H2O',
                ['H2', 'O']
            ],
            [
                'Mg(OH)2',
                ['Mg', '(OH)2']
            ],
            [
                'K4[ON(SO3)2]2',
                ['K4', '[ON(SO3)2]2']
            ],
            [
                'ON(SO3)2',
                ['O', 'N', '(SO3)2']
            ],
            [
                'SO3',
                ['S', 'O3']
            ]
        ];
    }

    /**
     * @dataProvider explodeFormulaProvider
     */
    public function testExplodeFormula_success($formula, $expected)
    {
        $molecule = new MoleculeService();

        $result = $molecule->explodeFormula($formula);

        $this->assertEquals($result, $expected);
    }

    public function isolatedCoefficientAndFormulaProvider()
    {
        return [
            [
                '(OH)2',
                [
                    'formula' => 'OH',
                    'coefficient' => 2
                ]
            ],
            [
                '[ON(SO3)2]2',
                [
                    'formula' => 'ON(SO3)2',
                    'coefficient' => 2
                ]
            ],
            [
                '{SO3}2',
                [
                    'formula' => 'SO3',
                    'coefficient' => 2
                ]
            ]
        ];
    }

    /**
     * @dataProvider isolatedCoefficientAndFormulaProvider
     */
    public function testIsolatedCoefficientAndFormula_success($formula, $expected)
    {
        $molecule = new MoleculeService();

        $result = $molecule->isolatedCoefficientAndFormula($formula);

        $this->assertEquals($result, $expected);
    }

    public function parseProvider()
    {
        return [
            [
                ['O', 'H'],
                1,
                [
                    'O' => 1,
                    'H' => 1
                ]
            ],
            [
                ['O', 'H'],
                2,
                [
                    'O' => 2,
                    'H' => 2
                ]
            ],
            [
                ['H2', 'O'],
                1,
                [
                    'H' => 2,
                    'O' => 1
                ]
            ],
            [
                ['O', 'N', '(SO3)2'],
                2,
                [
                    'O' => 14,
                    'N' => 2,
                    'S' => 4
                ]
            ]
        ];
    }

    /**
     * @dataProvider parseProvider
     */
    public function testParse_success($matches, $coefficient, $expected)
    {
        $molecule = new MoleculeService();

        $result = $molecule->parse($matches, $coefficient);

        $this->assertEquals($result, $expected);
    }
}
