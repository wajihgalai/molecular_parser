FROM alpine:3.7

RUN echo "ipv6" >> /etc/modules

RUN apk add --no-cache curl \
                       git \
                       s6 \
                       nginx \
                       php7 \
                       php7-fpm \
                       php7-openssl \
                       php7-mbstring \
                       php7-tokenizer \
                       php7-xml \
                       php7-json \
                       php7-phar \
                       php7-dom \
                       php7-xmlwriter \
                       php7-curl \
                       php7-xdebug && \
    # composer
    curl -L -s -f  -o /tmp/installer.php https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer && \
    php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer && \
    composer --ansi --version --no-interaction && \
    rm -rf /tmp/*

ADD infra/docker/nginx.conf /

ADD infra/docker/php-fpm.conf /

ADD infra/docker/s6/ /s6

ADD infra/docker/app-nginx.conf /

ADD infra/docker/entrypoint.sh /

ADD app/ /app

WORKDIR /app

RUN chmod +x /entrypoint.sh && \
    chown nginx:nginx -R /app && \
    composer install --no-interaction --prefer-dist && \
    composer dump-autoload --optimize

ENTRYPOINT ["/entrypoint.sh"]

CMD ["dev"]

EXPOSE 80
